/*
 * Copyright (c) 2023, Oracle and/or its affiliates.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a
 * copy of this software and associated documentation files (the "Software"),
 * to deal in the Software without restriction, including without limitation
 * the rights to use, copy, modify, merge, publish, distribute, sublicense,
 * and/or sell copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice (including the next
 * paragraph) shall be included in all copies or substantial portions of the
 * Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL
 * THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
 * DEALINGS IN THE SOFTWARE.
 */

#ifndef _DAMAGESCOPE_H_
#define _DAMAGESCOPE_H_

#define DAMAGEREQUESTHEADER  "DAMAGEREQUEST"
#define DAMAGEREPLYHEADER    "DAMAGEREPLY"
#define DAMAGEEVENTHEADER    "DAMAGEEVENT"

/*
  Aliases for types for Damage to x11.h types - used for types we don't
  have specialized printing routines for now, but may want to someday.
*/
#define DAMAGE                CARD32          /* XID */
#define REGION                CARD32          /* XID */

/*
  To aid in making the choice between level 1 and level 2, we
  define the following define, which does not print relatively
  unimportant fields.
*/

#define printfield(a,b,c,d,e) if (Verbose > 1) PrintField(a,b,c,d,e)

/* Damage 1.0 */
extern void DamageQueryVersion(FD fd, const unsigned char *buf);
extern void DamageQueryVersionReply(FD fd, const unsigned char *buf);

extern void DamageCreate(FD fd, const unsigned char *buf);
extern void DamageDestroy(FD fd, const unsigned char *buf);
extern void DamageSubtract(FD fd, const unsigned char *buf);

extern void DamageNotifyEvent(const unsigned char *buf);

extern void DamageBadDamageError(FD fd, const unsigned char *buf);

/* Damage 1.1 addition */
extern void DamageAdd(FD fd, const unsigned char *buf);

#endif

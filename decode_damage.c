/*
 * Copyright (c) 2023, Oracle and/or its affiliates.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a
 * copy of this software and associated documentation files (the "Software"),
 * to deal in the Software without restriction, including without limitation
 * the rights to use, copy, modify, merge, publish, distribute, sublicense,
 * and/or sell copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice (including the next
 * paragraph) shall be included in all copies or substantial portions of the
 * Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL
 * THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
 * DEALINGS IN THE SOFTWARE.
 */


#include <stdio.h>
#include <X11/X.h>
#include <X11/Xproto.h>
#include "scope.h"
#include "x11.h"
#include "damagescope.h"
#include "extensions.h"

static unsigned char DAMAGERequest, DAMAGEError, DAMAGEEvent;

#define DAMAGENError 1 /* as of DAMAGE 1.1 */

static void
damage_decode_req(FD fd, const unsigned char *buf)
{
    short Major = IByte(&buf[0]);
    short Minor = IByte(&buf[1]);

    switch (Minor) {
/* Damage 1.0 requests */
    case 0:
        DamageQueryVersion(fd, buf);
        ExtendedReplyExpected(fd, Major, Minor);
        break;
    case 1:
        DamageCreate(fd, buf);
        break;
    case 2:
        DamageDestroy(fd, buf);
        break;
    case 3:
        DamageSubtract(fd, buf);
        break;
/* Damage 1.1 addition */
    case 4:
        DamageAdd(fd, buf);
        break;

    default:
        ExtendedRequest(fd, buf);
        ExtendedReplyExpected(fd, Major, Minor);
        break;
    }
}

static void
damage_decode_reply(FD fd, const unsigned char *buf, short RequestMinor)
{
    switch (RequestMinor) {
/* Damage 1.0 replies */
    case 0:
        DamageQueryVersionReply(fd, buf);
        break;

    default:
        UnknownReply(buf);
        break;
    }
}

static void
damage_decode_event(FD fd, const unsigned char *buf)
{
    short event = IByte(&buf[0]) - DAMAGEEvent;

    switch (event) {
/* Damage 1.0 events */
    case 0:
        DamageNotifyEvent(buf);
        break;

    default:
        UnknownEvent(buf);
        break;
    }
}

static void
damage_decode_error(FD fd, const unsigned char *buf)
{
    short error = IByte(&buf[1]) - DAMAGEError;

    switch (error) {
/* Damage 1.0 error */
    case 0:
        DamageBadDamageError(fd, buf);
        break;

    default:
        UnknownError(buf);
        break;
    }
}

void
InitializeDAMAGE(const unsigned char *buf)
{
    TYPE p;

    DAMAGERequest = (unsigned char) (buf[9]);
    DAMAGEEvent = (unsigned char) (buf[10]);
    DAMAGEError = (unsigned char) (buf[11]);

    DefineEValue(&TD[REQUEST], (unsigned long) DAMAGERequest, "DamageRequest");
    DefineEValue(&TD[REPLY], (unsigned long) DAMAGERequest, "DamageReply");
    DefineEValue(&TD[EVENT], (unsigned long) DAMAGEEvent, "DamageNotify");

    DefineEValue(&TD[ERROR], (unsigned long) DAMAGEError + 0, "BadDamage");

    p = DefineType(DAMAGEREQUEST, ENUMERATED, "DAMAGEREQUEST",
                   (PrintProcType) PrintENUMERATED);
    DefineEValue(p, 0L, "DamageQueryVersion");
    DefineEValue(p, 1L, "DamageCreate");
    DefineEValue(p, 2L, "DamageDestroy");
    DefineEValue(p, 3L, "DamageSubtract");
    DefineEValue(p, 4L, "DamageAdd");
    
    p = DefineType(DAMAGEREPLY, ENUMERATED, "DAMAGEREPLY",
                   (PrintProcType) PrintENUMERATED);
    DefineEValue(p, 0L, "QueryVersion");

    p = DefineType(DAMAGEEVENT, ENUMERATED, "DAMAGEEVENT",
                   (PrintProcType) PrintENUMERATED);
    DefineEValue(p, 0L, "DamageNotify");

    p = DefineType(DAMAGEREPORTLEVEL, ENUMERATED, "DAMAGEREPORTLEVEL",
                   (PrintProcType) PrintENUMERATED);
    DefineEValue(p, 0L, "ReportRawRectangles");
    DefineEValue(p, 1L, "ReportDeltaRectangles");
    DefineEValue(p, 2L, "ReportBoundingBox");
    DefineEValue(p, 3L, "ReportNonEmpty");
    DefineEValue(p, 0x80L, "ReportRawRectangles & DamageNotifyMore");
    DefineEValue(p, 0x81L, "ReportDeltaRectangles & DamageNotifyMore");
    DefineEValue(p, 0x82L, "ReportBoundingBox & DamageNotifyMore");
    DefineEValue(p, 0x83L, "ReportNonEmpty & DamageNotifyMore");

    InitializeExtensionDecoder(DAMAGERequest, damage_decode_req,
                               damage_decode_reply);
    InitializeExtensionEventDecoder(DAMAGEEvent, damage_decode_event);
    InitializeExtensionErrorDecoder(DAMAGEError, damage_decode_error);
}
